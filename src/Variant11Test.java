import org.junit.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class Variant11Test {

    @Test(dataProvider = "modSumProvider")
    public void testModSum(double a, double b, double res)
    {
        Assert.assertEquals(res, Variant11.modSum(a, b), 0);
    }

    @DataProvider
    public Object[][] modSumProvider()
    {
        return new Object[][] {{2, 3, 5}, {-1, 2, 3}, {4, -3, 7}, {-4, -2, 6}};
    }

    @Test(dataProvider = "modDiffProvider")
    public void testModDiff(double a, double b, double res)
    {
        Assert.assertEquals(res, Variant11.modDiff(a, b), 0);
    }

    @DataProvider
    public Object[][] modDiffProvider()
    {
        return new Object[][] {{2, 3, -1}, {-1, 2, -1}, {4, -3, 1}, {-4, -2, 2}};
    }

    @Test(dataProvider = "modOpProvider")
    public void testModOp(double a, double b, double res)
    {
        Assert.assertEquals(res, Variant11.modOp(a, b), 0);
    }

    @DataProvider
    public Object[][] modOpProvider()
    {
        return new Object[][] {{2, 3, 6}, {-1, 2, 2}, {4, -3, 12}, {-4, -2, 8}};
    }

    @Test(dataProvider = "modFracProvider")
    public void testModFrac(double a, double b, double res) throws Exception
    {
        Assert.assertEquals(res, Variant11.modFrac(a, b), 0);
    }

    @DataProvider
    public Object[][] modFracProvider()
    {
        return new Object[][] {{2, 3, 2.0/3}, {-1, 2, 0.5}, {4, -3, 4.0/3}, {-4, -2, 2}};
    }

    @Test(dataProvider = "numSumProvider")
    public void testNumSum(int a, int res)
    {
        Assert.assertEquals(res, Variant11.numSum(a));
    }

    @DataProvider
    public Object[][] numSumProvider()
    {
        return new Object[][] {{123, 6}, {525, 12}, {-144, 9}, {100, 1}};
    }

    @Test(dataProvider = "numOpProvider")
    public void testNumOp(int a, int res)
    {
        Assert.assertEquals(res, Variant11.numOp(a));
    }

    @DataProvider
    public Object[][] numOpProvider()
    {
        return new Object[][] {{123, 6}, {525, 50}, {-144, 16}, {100, 0}};
    }

    @Test(dataProvider = "parityProvider")
    public void testParity(int a, int b, boolean res)
    {
        Assert.assertEquals(res, Variant11.parity(a, b));
    }

    @DataProvider
    public Object[][] parityProvider()
    {
        return new Object[][] {{2, 3, false}, {4, 4, true}, {-2, -2, true}, {-3, 6, false}};
    }

    @Test(dataProvider = "maxOrZeroProvider")
    public void testMaxOrZero(int a, int b, Pair res)
    {
        Assert.assertEquals(res, Variant11.maxOrZero(a, b));
    }

    @DataProvider
    public Object[][] maxOrZeroProvider()
    {
        return new Object[][] {{2, 3, new Pair(3, 3)}, {4, 4, new Pair(0, 0)}, {-2, -2, new Pair(0, 0)}, {-3, 6, new Pair(6, 6)}};
    }

    @Test(dataProvider = "locatorProvider")
    public void testLocator(char c, int n1, int n2, char res)
    {
        Assert.assertEquals(res, Variant11.locator(c, n1, n2));
    }

    @DataProvider
    public Object[][] locatorProvider()
    {
        return new Object[][] {{'N', 1, 1, 'S'}, {'W', 1, -1, 'W'}, {'N', 2, -1, 'W'}, {'S', 2, 2, 'S'}};
    }

    @Test(dataProvider = "sumNProvider")
    public void testSumN(int a, int res)
    {
        Assert.assertEquals(res, Variant11.sumN(a));
    }

    @DataProvider
    public Object[][] sumNProvider()
    {
        return new Object[][] {{1, 5}, {2, 29}, {3, 86}, {5, 355}};
    }

    @Test(dataProvider = "sumKProvider")
    public void testSumK(int a, Pair res)
    {
        Assert.assertEquals(res, Variant11.sumK(a));
    }

    @DataProvider
    public Object[][] sumKProvider()
    {
        return new Object[][] {{6, new Pair(3, 6)}, {5, new Pair(3, 6)}, {4, new Pair(3, 6)}, {2, new Pair(2, 3)}, {10, new Pair(4, 10)}};
    }

    @Test(dataProvider = "minProvider")
    public void testMin(int a[], int res) throws Exception
    {
        Assert.assertEquals(res, Variant11.min(a));
    }

    @DataProvider
    public Object[][] minProvider()
    {
        return new Object[][] {{new int[]{1, 2, 3, 4, 5}, 3}, {new int[]{4, 3, 2, 5, -2, 4, -3, -1}, -3}, {new int[]{0, 0, 0, -1, 5, 10, -2, 3}, -2}};
    }

    @Test(dataProvider = "matrixProvider")
    public void testMatrix(int a[][], int res[][])
    {
        Assert.assertArrayEquals(res, Variant11.matrix(a));
    }

    @DataProvider
    public Object[][] matrixProvider()
    {
        return new Object[][] {{new int[][]{{1, 2}, {3, 4}}, new int[][]{{4, 2}, {3, 1}}}, {new int[][]{{1, 2, 3, 4}, {5, 6, 7, 8}}, new int[][]{{7, 8, 3, 4}, {5, 6, 1, 2}}},
                               {new int[][]{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}}, new int[][]{{11, 12, 3, 4}, {15, 16, 7, 8}, {9, 10, 1, 2}, {13, 14, 5, 6}}}};
    }
}