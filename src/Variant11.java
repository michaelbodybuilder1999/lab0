public class Variant11 {

    public static double modSum(double a, double b)
    {
        return Math.abs(a) + Math.abs(b);
    }

    public static double modDiff(double a, double b)
    {
        return Math.abs(a) - Math.abs(b);
    }

    public static double modOp(double a, double b)
    {
        return Math.abs(a) * Math.abs(b);
    }

    public static double modFrac(double a, double b) throws Exception
    {
        if(b != 0)
            return Math.abs(a) / Math.abs(b);
        throw new Exception();
    }

    public static int numSum(int a)
    {
        int sum = 0;

        while(a != 0)
        {
            sum += a % 10;

            a = a / 10;
        }

        return Math.abs(sum);
    }

    public static int numOp(int a)
    {
        int op = 1;

        while(a != 0)
        {
            op *= a % 10;

            a = a / 10;
        }

        return Math.abs(op);
    }

    public static boolean parity(int a, int b)
    {
        return (a % 2 == b % 2);
    }

    private static int max(int a, int b)
    {
        if(a >= b)
            return a;
        return b;
    }

    public static Pair maxOrZero(int a, int b)
    {
        if(a != b)
            a = b = max(a, b);
        else
            a = b = 0;

        return new Pair(a, b);
    }

    public static char locator(char c, int n1, int n2)
    {
        switch(n1)
        {
            case 1:
            {
                switch(c)
                {
                    case 'N': c = 'W'; break;
                    case 'W': c = 'S'; break;
                    case 'S': c = 'E'; break;
                    case 'E': c = 'N'; break;
                }
                break;
            }
            case -1:
            {
                switch(c)
                {
                    case 'N': c = 'E'; break;
                    case 'W': c = 'N'; break;
                    case 'S': c = 'W'; break;
                    case 'E': c = 'S'; break;
                }
                break;
            }
            case 2:
            {
                switch(c)
                {
                    case 'N': c = 'S'; break;
                    case 'W': c = 'E'; break;
                    case 'S': c = 'N'; break;
                    case 'E': c = 'W'; break;
                }
                break;
            }
        }

        switch(n2)
        {
            case 1:
            {
                switch(c)
                {
                    case 'N': c = 'W'; break;
                    case 'W': c = 'S'; break;
                    case 'S': c = 'E'; break;
                    case 'E': c = 'N'; break;
                }
                break;
            }
            case -1:
            {
                switch(c)
                {
                    case 'N': c = 'E'; break;
                    case 'W': c = 'N'; break;
                    case 'S': c = 'W'; break;
                    case 'E': c = 'S'; break;
                }
                break;
            }
            case 2:
            {
                switch(c)
                {
                    case 'N': c = 'S'; break;
                    case 'W': c = 'E'; break;
                    case 'S': c = 'N'; break;
                    case 'E': c = 'W'; break;
                }
                break;
            }
        }

        return c;
    }

    public static int sumN(int n)
    {
        int sum = 0;

        for(int i = 0; i <= n; ++i)
        {
            sum += Math.pow(n + i, 2);
        }

        return sum;
    }

    public static Pair sumK(int n)
    {
        int sum = 1, k = 1;

        while(sum < n)
        {
            ++k;
            sum += k;
        }

        return new Pair(k, sum);
    }

    public static int min(int[] a) throws Exception
    {
        if(a.length <= 2)
            throw new Exception();

        int min = a[2];

        for(int i = 4; i < a.length; i += 2)
        {
            if(a[i] < min)
            {
                min = a[i];
            }
        }

        return min;
    }

    public static int[][] matrix(int[][] a)
    {
        for(int i = 0; i < a.length / 2; ++i)
        {
            for(int j = 0; j < a[i].length / 2; ++j)
            {
                int buf = a[i][j];

                a[i][j] = a[i + a.length / 2][j + a[i].length / 2];
                a[i + a.length / 2][j + a[i].length / 2] = buf;
            }
        }

        return a;
    }
}

class Pair {
    public int first, second;

    public Pair(int first, int second)
    {
        this.first = first;
        this.second = second;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj.getClass() == Pair.class)
        {
            Pair pair = (Pair) obj;
            return first == pair.first && second == pair.second;
        }
        else
            return false;
    }
}
